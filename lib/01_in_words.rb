class Fixnum
  def in_words
    numbers_to_name = {10**18 => "quintillion", 10**15 => "quadrillion", 10**12 => "trillion",1000000000 => "billion",
      1000000 => "million",
      1000 => "thousand",
      100 => "hundred",
      90 => "ninety",
      80 => "eighty",
      70 => "seventy",
      60 => "sixty",
      50 => "fifty",
      40 => "forty",
      30 => "thirty",
      20 => "twenty",
      19=>"nineteen",
      18=>"eighteen",
      17=>"seventeen",
      16=>"sixteen",
      15=>"fifteen",
      14=>"fourteen",
      13=>"thirteen",
      12=>"twelve",
      11 => "eleven",
      10 => "ten",
      9 => "nine",
      8 => "eight",
      7 => "seven",
      6 => "six",
      5 => "five",
      4 => "four",
      3 => "three",
      2 => "two",
      1 => "one"
    }
    str = ""
    numbers_to_name.each do |num, name|
      if self == 0
        return "zero"
      elsif self.to_s.length == 1 && self/num > 0
        return "#{name}"
      elsif self < 100 && self/num > 0
        return str + "#{name}" if self % num == 0
        return str + "#{name} " + numbers_to_name[self%num]
      elsif self/num > 0
        return (self/num).in_words + " #{name}" if (self%num).in_words == "zero"
        return str + (self/num).in_words + " #{name} " + (self%num).in_words
      end
    end
  end
end
